function sortCarByYearDescendingly(cars) {
  // Sangat dianjurkan untuk console.log semua hal hehe
  console.log(cars);
  var done = false;
  // Clone array untuk menghindari side-effect
  // Apa itu side effect?
  const result = [...cars];
  while (!done) {
    done = true;
    //console.log(typeof result)
    for(let i=1;i<result.length;i+=1){
      if(result[i-1].year<result[i].year){
        done=false;
      var tmp = result[i-1];
      result[i-1] = result[i];
      result[i] = tmp;
    //console.log(tmp)  
    //console.log(result[i].year)
    }
    }
  }
  // Tulis code-mu disini
  result.sort((a,b)=>b.year-a.year)
  // Rubah code ini dengan array hasil sorting secara descending
  return result;
}
